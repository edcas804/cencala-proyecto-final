var menu = document.querySelector('#toggleMenu');
var toggleMenu= document.querySelector('#toggle-menu');
var closeMenu= document.querySelector('#close-menu');

const showHide = () => {
	menu.classList.toggle("showHide");
	let status = true;
	localStorage.setItem('menuStatus', status);
}

toggleMenu.addEventListener('click', showHide);
closeMenu.addEventListener('click', showHide);
